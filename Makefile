# Put your source files here (or *.c, etc)
SRCS=main.c stm32f30x_it.c

# Binaries will be generated with this name (.elf, .bin, .hex, etc)
PROJ_NAME=blinky

ARM_TOOLCHAIN_DIR=/opt/gcc-arm-none-eabi-4_9-2015q2/bin/

# Normally you shouldn't need to change anything below this line!
#######################################################################################
CC=$(ARM_TOOLCHAIN_DIR)/arm-none-eabi-gcc
CPP=$(ARM_TOOLCHAIN_DIR)/arm-none-eabi-g++
AS=$(ARM_TOOLCHAIN_DIR)/arm-none-eabi-as
OBJCOPY=$(ARM_TOOLCHAIN_DIR)/arm-none-eabi-objcopy
GDB=$(ARM_TOOLCHAIN_DIR)/arm-none-eabi-gdb

STM_COMMON=STM32F30x_StdPeriph_Driver/

CFLAGS  = -g -Os -Wall
CFLAGS += -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork
CFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
CFLAGS += -I.
#CFLAGS += -flto -ffunction-sections -fdata-sections -fno-builtin

# Include files from STM libraries
CFLAGS += -I./CMSIS/Include
CFLAGS += -I./CMSIS/Device/ST/STM32F30x/Include
CFLAGS += -I$(STM_COMMON)/inc

LDFLAGS  = -Tstm32f3_flash.ld
LDFLAGS += -Wl,--gc-sections
LDFLAGS += --specs=nano.specs
LDFLAGS += --specs=nosys.specs
LDFLAGS += -Wl,-Map=output.map

# Add a few StdPeriph files that we need
SRCS += ./CMSIS/Device/ST/STM32F30x/Source/Templates/system_stm32f30x.c
SRCS += stm32f3_discovery.c
SRCS += $(STM_COMMON)/src/stm32f30x_exti.c
SRCS += $(STM_COMMON)/src/stm32f30x_gpio.c
SRCS += $(STM_COMMON)/src/stm32f30x_misc.c
SRCS += $(STM_COMMON)/src/stm32f30x_rcc.c
SRCS += $(STM_COMMON)/src/stm32f30x_syscfg.c

SSRCS = ./CMSIS/Device/ST/STM32F30x/Source/Templates/TrueSTUDIO/startup_stm32f30x.s 

OBJS = $(SRCS:.c=.o)
SOBJS = $(patsubst %.s,%.k,$(SSRCS))

.PHONY: proj

proj: $(PROJ_NAME).elf

$(PROJ_NAME).elf: $(OBJS) $(SOBJS)
	$(CPP) $(CFLAGS) $(LDFLAGS) $^ -o $(PROJ_NAME).elf
	$(OBJCOPY) -O binary $(PROJ_NAME).elf $(PROJ_NAME).bin

debug-openocd: $(PROJ_NAME).elf
	$(GDB) $(PROJ_NAME).elf \
		-ex 'target extended-remote :3333' \
		-ex 'monitor reset halt' \
		-ex 'load' \
		-ex 'monitor reset halt' \
		-ex 'monitor arm semihosting enable'

debug-jlink: $(PROJ_NAME).elf
	$(GDB) $(PROJ_NAME).elf \
		-ex 'target extended-remote :2331' \
		-ex 'monitor speed 1000' \
		-ex 'monitor flash download = 1' \
		-ex 'monitor flash breakpoints = 1' \
		-ex 'monitor flash device = STM32F303VC' \
		-ex 'monitor endian little' \
		-ex 'monitor semihosting enable' \
		-ex 'monitor reset' \
		-ex 'load' \
		-ex 'monitor reset'

clean:
	rm -f $(OBJS) $(SOBJS) output.map \
		$(PROJ_NAME).elf $(PROJ_NAME).hex $(PROJ_NAME).bin

%.o: %.c
	$(CPP) $(CFLAGS) -c $< -o $@
%.k: %.s
	$(AS) $< -o $@
